#!/usr/bin/env python3

import setuptools, sys


setuptools.setup(
	name = 'myuzi',
	version = '1.3.2',
	author = 'zehkira',
	description = 'Modules for Myuzi',
	url = 'https://gitlab.com/zehkira/myuzi',
	packages = setuptools.find_packages(),
	install_requires = ['requests', 'beautifulsoup4'],
	python_requires = '>=3.9',
)
